import React, {Component} from 'react';
import {BrowserRouter,Route,Switch} from 'react-router-dom';
import Nosotros from './Nosotros';
import Error from './Error';
import Productos from './Productos';
import Header from './Header';
import SingleProducto from './SingleProducto';
import infoProductos from '../datos/datos.json';
import Navegacion from './Navegacion';
import Contacto from './Contacto';


class Router extends Component {

    state = {
        productos: [],
        terminoBusqueda : ''
    }

    componentWillMount(){
        this.setState({
            productos: infoProductos
        })
    }

    busquedaProducto = (busqueda) => {
        if(busqueda.length >3){
            this.setState({
                terminoBusqueda: busqueda
            })
        }else{
            this.setState({
                terminoBusqueda: ''
            })
        }
    }
 
    render() { 
        
        //copia del state
        let productos = [...this.state.productos];
        
        //termino que se va a buscar
        let busqueda = this.state.terminoBusqueda;

        let resultado;
       
        //busqueda para filtrar
        if(busqueda !== ''){
            resultado = productos.filter(producto => (
                producto.nombre.toLowerCase().indexOf(busqueda.toLowerCase()) !== -1
            ))
        }else{
            resultado = productos;
        }

        return (  
            <BrowserRouter>
            <div className="contenedor">
                <Header></Header>
                <Navegacion></Navegacion>
                <Switch>
                    <Route exact path="/" render={() => (<Productos productos={resultado} busquedaProducto={this.busquedaProducto}></Productos>)}></Route>
                    <Route exact path="/nosotros" component={Nosotros}></Route>
                    <Route exact path="/productos" render={ () => (<Productos productos={resultado} busquedaProducto={this.busquedaProducto}></Productos>)}></Route>
                    <Route exact path="/producto/:productoId" render={(props) => {
                        let idProducto = props.location.pathname.replace('/producto/','');
                        return(
                            <SingleProducto producto={this.state.productos[idProducto]}></SingleProducto>
                        )
                    }}></Route>
                    <Route exact path="/contacto" component={Contacto}></Route>
                    <Route component={Error}></Route>
                </Switch>
             </div>
            </BrowserRouter>
        );
    }
}
 
export default Router;