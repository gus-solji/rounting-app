import React, { Component } from 'react';
import './Nosotros.css';

class Nosotros extends Component {
    state = {  }
    render() { 
        return ( 
          <div className="contenedor-nosotros">
            <div className="imagen">
                <img src="/img/camisa_1.png" alt="imagen nosotros"></img>
            </div>
            <div className="contenido">
                <h2>Nosotros</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla est erat, accumsan sit amet tellus sit amet, tempor molestie diam. Curabitur magna nisi, venenatis sed metus vel, ultrices lacinia urna. Cras at lectus laoreet, congue lacus sit amet, venenatis justo. Nam gravida urna ac lacus pretium, vitae suscipit ipsum finibus. Sed tristique in quam sed malesuada. Nam venenatis lacinia tristique. Duis ullamcorper tellus vel velit varius, vel fermentum leo fringilla. Proin maximus sem ac leo vestibulum varius. Phasellus ultricies blandit est, eu tincidunt massa aliquet in. Nam vitae neque ornare, convallis augue ut, tincidunt mi. Etiam dapibus porta tortor a fermentum. Etiam maximus porta magna, et iaculis ipsum auctor vel.</p>
            </div>
          </div>
         );
    }
}
 
export default Nosotros;