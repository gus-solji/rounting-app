import React from 'react';
import './Contacto.css';

const Contacto = () => {
    return (
      <form>
          <legend>Formulario de Contacto</legend>
          <div className="input-field">
            <label>Nombre:</label>
            <input type="text" placeholder="Tu nombre"></input>
          </div>
          <div className="input-field">
            <label>Email:</label>
            <input type="email" placeholder="Tu email"></input>
          </div>
          <div className="input-field">
            <label>Mensaje:</label>
            <textarea></textarea>
          </div>
          <div className="input-field enviar">
            <input type="submit" value="Enviar"></input>
          </div>
      </form>
    );
};

export default Contacto;